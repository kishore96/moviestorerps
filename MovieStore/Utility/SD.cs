﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieStore.Utility
{
    public static class SD
    {
        public static readonly byte Unknown = 0;
        public static readonly byte PayAsYouGo = 1;

        public const string CanManageMovies = "CanManageMovies";
    }
}