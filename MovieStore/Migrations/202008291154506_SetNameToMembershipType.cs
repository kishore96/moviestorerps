namespace MovieStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetNameToMembershipType : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE MembershipTypes SET Name = 'Pay as You Go' WHERE iD = 1");
            Sql("UPDATE MembershipTypes SET Name = 'Monthly' WHERE iD = 2");
            Sql("UPDATE MembershipTypes SET Name = 'Quartely' WHERE iD = 3");
            Sql("UPDATE MembershipTypes SET Name = 'Annualy' WHERE iD = 4");

        }

        public override void Down()
        {
        }
    }
}
