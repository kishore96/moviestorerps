namespace MovieStore.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeedUsers : DbMigration
    {
        public override void Up()
        {
            Sql(@"
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES ('62f4ee2e-9469-4293-becc-43cd9fe530ca', 'admin@moviestore.com', 0, 'AFys3SHoR9UwN6MyZV07IMsgujAATvMKMwDlYlLkzCPyVWJaC5eAHjxug6hacyH8hA==', '5c470df6-4e3e-48e0-b8bd-469613150bb8', NULL, 0, 0, NULL, 1, 0, 'admin@moviestore.com')
INSERT INTO [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES ('b2f767ad-0cc2-4a59-a21a-b14a676df48b', 'Guest@moviestore.com', 0, 'AP6thfY3/je5NaEh8fU+LCrShvCU6xi/uzuvq9pFkf4092ApIyUke3TRJTHM7VExgA==', 'de3fdbbd-5dbd-45fc-b9a4-ef14c56c0c77', NULL, 0, 0, NULL, 1, 0, 'Guest@moviestore.com')

INSERT INTO [dbo].[AspNetRoles] ([Id], [Name]) VALUES ('9d73e83c-41a4-462e-9b6f-d20f3cd79dab', 'CanManageMovies')

INSERT INTO [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES ('62f4ee2e-9469-4293-becc-43cd9fe530ca', '9d73e83c-41a4-462e-9b6f-d20f3cd79dab')
");
        }
        
        public override void Down()
        {
        }
    }
}
