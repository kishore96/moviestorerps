﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MovieStore.Dtos
{
    public class MembershipTypeDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}